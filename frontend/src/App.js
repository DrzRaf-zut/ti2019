import React from 'react';
import {connect} from "react-redux";
import {simpleAction} from "./actions/simpleAction";
import TopBar from "./components/TopBar";
import { BrowserRouter as Router, Route } from "react-router-dom";
import MainPage from "./components/MainPage/MainPage";
import LoginPage from "./components/LoginPage/LoginPage";
import RegisterPage from "./components/RegisterPage/LoginPage";

const App = ({res, simpleAction}) => (
    <Router>
        <TopBar />
        <Route path="/" exact component={MainPage} />
        <Route path="/login" component={LoginPage} />
        <Route path="/register" component={RegisterPage} />
    </Router>
);

const mapStateToProps = (state) => ({
    res: state.simpleReducer.result
})

const mapDispatchToProps = {
    simpleAction
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
