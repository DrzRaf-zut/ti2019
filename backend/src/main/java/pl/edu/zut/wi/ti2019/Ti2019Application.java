package pl.edu.zut.wi.ti2019;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ti2019Application {

    public static void main(String[] args) {
        SpringApplication.run(Ti2019Application.class, args);
    }

}
